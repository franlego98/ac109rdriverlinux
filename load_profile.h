/*
 * load_profile.h
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
 
#ifndef _LOAD_PROFILE_H
#define _LOAD_PROFILE_H
 
#include <jansson.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define KEYBOARD_NUM_KEYS	131

typedef struct {
	u_int8_t red;
	u_int8_t green;
	u_int8_t blue;
	u_int8_t alpha;
} keyboard_key;

typedef struct {
	unsigned int code;
	char *name;
} dict_obj;

int parseJson(const char * filename,keyboard_key *lista);
int get_num_key(const char * name);

#endif _LOAD_PROFILE_H
