/*
 * tools_stream.c
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
#include "tools_stream.h"

int stream_init(stream_data *s,int profile){
	memset(s,0,sizeof(stream_data));
	s->profile = profile;
	return 0;
}

int stream_send(libusb_device_handle **handle,stream_data *s,unsigned char *msg,int siz){
	int i = 0;
	int res = 0;
	unsigned char *p1 = &s->buff,*p2 = msg;
	while(i < siz){
		if(s->buff_filled == STREAM_BUFF_MAX){
			unsigned char temp[6 + STREAM_BUFF_MAX];
			temp[0] = 0x27;
			temp[1] = 0x01 + s->profile;
			temp[2] = s->sent & 0xff;
			temp[3] = (s->sent & 0xff00) >> 8;
			temp[4] = (s->sent & 0xff0000) >> 16;
			temp[5] = STREAM_BUFF_MAX;
			
			int k = 0;
			while(k < STREAM_BUFF_MAX){
				temp[6+k] = s->buff[k];
				k++;
			}
			
			s->sent += STREAM_BUFF_MAX;
			s->buff_filled = 0;
			
			res = send_cmd(handle,&temp,6+STREAM_BUFF_MAX);
		
			k = 0;
			while(k < STREAM_BUFF_MAX){
				s->buff[k] = 0x00;
				k++;
			}
			
			if(res < 0)
				return res;
		}
		
		*(p1 + s->buff_filled) = *(p2+i);
		i++;
		s->buff_filled++;
	}
	
	return res;
}

int stream_end(libusb_device_handle **handle,stream_data *s){
	
	unsigned char temp[6 + STREAM_BUFF_MAX];
	temp[0] = 0x27;
	temp[1] = 0x01 + s->profile;
	temp[2] = s->sent & 0xff;
	temp[3] = (s->sent & 0xff00) >> 8;
	temp[4] = (s->sent & 0xff0000) >> 16;
	temp[5] = s->buff_filled;
	
	int k = 0;
	while(k < s->buff_filled){
		temp[6+k] = s->buff[k];
		k++;
	}
	
	return send_cmd(handle,&temp,62);
}
