/*
 * tools.h
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
 
#ifndef _TOOLS_H
#define _TOOLS_H 

#include <stdio.h>
#include "libusb-1.0/libusb.h"

typedef struct {
	u_int8_t code[6];
	u_int16_t magic;
	unsigned char ud[56];
} paquete;

int init_usb(libusb_device_handle **hand);
void close_usb(libusb_device_handle **hand);
int send_cmd(libusb_device_handle **hand,unsigned char *cmd,int size);
int do_ping(libusb_device_handle **hand);
paquete * build_pkt(unsigned char * cmd,size_t s);

#endif _TOOLS_H
