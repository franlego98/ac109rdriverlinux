/*
 * load_profile.c
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
 
#include "load_profile.h"
#include "keys_dict.h"
 
int parseJson(const char * filename,keyboard_key *lista){
	FILE *f = fopen(filename,"r");
	
	if(f <= 0){
		return -1;
	}
	
	json_error_t error;
	json_t *t = json_loadf(f,JSON_REJECT_DUPLICATES,&error);
	fclose(f);
	
	if(t == NULL){
		return -1;
	}
	
	if(!json_is_object(t)){
		//check that is a dictionary
		return -1;
	}
	
	char *main_key;
	json_t *main_val;
	
	json_object_foreach(t,main_key,main_val){
		//check the value is a key
		if(!json_is_array(main_val))
			return -1;
			
		keyboard_key temp;
		int s = json_array_size(main_val);
		int ok = -1;
		
		int pos = get_num_key(main_key);
		
		if(!(pos < 0)){
			if(s == 1){
				json_t *value = json_array_get(main_val,0);
				if(!json_is_integer(value))
					return -1;
					
				temp.red = temp.green = temp.blue = temp.alpha = json_integer_value(value) & 0xff;
				
				ok = 1;
			}
			
			if(s == 3){
				json_t *value_red = json_array_get(main_val,0);
				if(!json_is_integer(value_red))
					return -1;
					
				json_t *value_green = json_array_get(main_val,1);
				if(!json_is_integer(value_green))
					return -1;
					
				json_t *value_blue = json_array_get(main_val,2);
				if(!json_is_integer(value_blue))
					return -1;
					
				temp.red = json_integer_value(value_red) & 0xff;
				temp.green = json_integer_value(value_green) & 0xff;
				temp.blue = json_integer_value(value_blue) & 0xff;
				temp.alpha = 0xff;
				
				ok = 1;
			}
			
			if(s == 4){
				json_t *value_red = json_array_get(main_val,0);
				if(!json_is_integer(value_red))
					return -1;
					
				json_t *value_green = json_array_get(main_val,1);
				if(!json_is_integer(value_green))
					return -1;
					
				json_t *value_blue = json_array_get(main_val,2);
				if(!json_is_integer(value_blue))
					return -1;
					
				json_t *value_alpha = json_array_get(main_val,3);
				if(!json_is_integer(value_alpha))
					return -1;
					
				temp.red = json_integer_value(value_red) & 0xff;
				temp.green = json_integer_value(value_green) & 0xff;
				temp.blue = json_integer_value(value_blue) & 0xff;
				temp.alpha = json_integer_value(value_alpha) & 0xff;
				
				ok = 1;
			}	
		
			if(ok == 1){
				memcpy(&lista[pos],&temp,sizeof(keyboard_key));
			}
		}
	}
	
	return 0;
}

int get_num_key(const char * name){
	int i = 0;
	int res = -1;
	dict_obj *temp = &keys_dict_es;
	while(temp->code != -1){
		if(!strcasecmp(temp->name,name)){
			res = temp->code;
		}
		temp = &keys_dict_es[i++];
	}
	
	return res;
}
