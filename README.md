THIS SOFTWARE IS DISTRIBUTED "AS IS", WITHOUY ANY WARRANTY. LICENSED AS GNU GPL V3.0

# ACGAM AG109R Light Driver Linux

The goal of this project is to add support for the mechanical keyboard Acgam AC-109R. This is a UNOFFICIAL project and its completly independent of Acgam. This product is basically a keyboard with an RGB led in every key so you can program every single key. Officially its only supported on Windows.

All the information has been done with reverse engineering, sniffing traffic on a Windows machine and trying to understand de traffic. The program *acgamd* is basically the implementation of the knowledge obtained, which is described in next lines.

# AG109R Light Protocol

The keyboard has 3 USB interfaces, the number 1 (starting from 0) is used for the light control. The comunication is mainly command-oriented but it have a *stream* mode to do large data transfers. Out endpoint is 0x04 and in endpoint is 0x83. I've identified a structure in the USB messages.

As I know the data is send as bytes and nothing about ASCII. Packets are 64 bytes len and they have the next structure (*tools.h*):

    struct {
        unsigned char   first[6];
        uint16_t        crc;
        unsigned char   second[56];
    } paquete;
    
The crc value is a CRC (CCITT start value 0xffff) of the whole packect with the crc value at zero. If this value is incorrect the keyboard won't reply. The concrete CRC algoritm has been found by trying different typical checksums algorithms (All thanks to libcrc by Lammert Bies, which code is a critical part of this program).

Using this format, few commands are used.

**Light parameters**
Each led admits four 8bit values: red, green, blue and alpha.

# AG109 Commands

**Ping**
Just a ping pong utility. This is used by the Windows driver periodically. This is only a message with the first byte "0x0c" and the rest at zero, with the checksum. The keyboard simply repeats this message.
    
    `paquete.first[0] = 0x0c;`

**Set profile**
The keyboard has 3 customizable profiles, each profile has a complete set of color for each key. To choose one you can do it with the keyboard or with a command, which is a message with the first byte "0x0b" and the second with the number of the profile plus one.
    
    `paquete.first[0] = 0x0b;
    paquete.first[1] = 0x01 + profile;`
    
**Clear profile**
This is very similar to the set profile command. It switch off all the lights of the profile.

    `paquete.first[0] = 0x21;
    paquete.first[1] = 0x01 + profile;
    paquete.first[2] = 0x06;`

**Stream data**
To make the transfer of profiles its uses a protocol over messages. It allow to perfom ordered transfers of data larger than 62 bytes. To do this, it reserves few bytes to add an offset and a len of the data transmitted. The implementation of this transmission is in *tools_stream.c*.

For me is now a mistery some parts of the data transmited over this protocol. Althougth I've find the way to transfer profiles and modify the color of each key, I obtanined it by seeing differences between different colors transfers. I think this stream could be a kind of code of a program executed by the keybaord. I think that due to there are some keys (º and w) in the profile 1 which perform an animation, and its transfered later than the keys colors. I've not be able to identify anything in this data but I found that writing all with zeroes (which makes a simpler and more elegant code) the animation is put the lights off for one or two seconds and then back to normality. Without this filling, the animation consists in switch off the lights for a long time. Could be interserting to investigate this.

To load a profile the procedure is:

* Clear profile
* Open stream
* Send a concrete sequence of characters (copied from traffic)
* Send 116 key data (R G B Alpha , one byte per value) without any effect (its looks like it do a & operation with the following key color data)
* Another concrete sequence of characters
* Send 131 key data. The keys are organised as a array data, each data as 4 byte (R G B A). Some of this positions doesnt correspond to any key, its looks like it fill a matrix of some keys (like the space bar) fills serveral keys, so this keys are not real but can be programmed. All the positions of the matrix are in *keys_dict.h* , at this moment for the spanish layout.
* A sequence of zeroes (the exact size its in the code)
* Close stream
* Set profile


# Profiles description
Custom profiles are saved as a JSON file. This file is a dictionary where keys are names of each keyboard key and the value is a array of data.

The arrays can have 1,3 or 4 values.

With one value, this value is put in R,G,B,A values. This is white brightness.

With three values, each value is for a color. Ordered by R,G,B.

With four values is the same than three but the last is the alpha value.

# Command usage
As explained in the command help:

    `Keyboard ACGAM AG-109R Light Driver for Linux v1.0
    
     Usage:
             set-profile   N                  -  Select profile N in the keyboard                                                           
             clear-profile N                  -  Clear profile N in the keyboard                                                            
             load-profile  N json_file        -  Load $json_file in profile N                                                               
             fill-profile  N B|{R G B}        -  Fill all profile N with brighness B or                                                     
                                                 with color R G B
    `