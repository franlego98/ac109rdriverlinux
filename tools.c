/*
 * tools.c
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
 
#include "libcrc/crcccitt.c"
#include "tools.h"
#include "parameters.h"

int init_usb(libusb_device_handle **hand){
		
	int n;
	n = libusb_init(NULL);
	if(n < 0){
		printf("Error initializating LIBUSB\n");
		return n;
	}
	
	*hand = libusb_open_device_with_vid_pid(NULL,KEYBOARD_VID,KEYBOARD_PID);
	
	if(*hand == NULL){
		printf("Error when finding device! Check conection\n");	
		return -1;
	}
	
	libusb_detach_kernel_driver(*hand,LIGHT_USB_INTERFACE);
	n = libusb_claim_interface(*hand,LIGHT_USB_INTERFACE);
	
	if(n < 0){
		printf("Error when opening device! Are you sudo?\n");
		return n;
	}
	
	return n;
}

void close_usb(libusb_device_handle **hand){
	libusb_close(*hand);
	libusb_exit(NULL);
}

int send_cmd(libusb_device_handle **hand,unsigned char *cmd,int size){
		int n,exp;
		unsigned char recv[BUFF_LEN];
		
		paquete *send = build_pkt(cmd,size);
		
		/*int j = 0;
		unsigned char *p = send;
		while(j < 64){
			printf("0x%x ",*p);
			j++;
			p++;
		}
		
		putc('\n',stdout);*/
				
		n = libusb_interrupt_transfer(*hand,KEYBOARD_ENDPOINT,send,BUFF_LEN,&exp,0);
		if(n < 0){
			printf("Failed to transmit\n");
			return n;
		}
		
		n = libusb_interrupt_transfer(*hand,HOST_ENDPOINT,&recv,BUFF_LEN,&exp,0);
		if(n < 0){
			printf("Failed to receive response\n");
		}
		
		
		if(exp != BUFF_LEN){
			printf("Incomplete response\n");
		}
		
		return n;
}

paquete * build_pkt(unsigned char *cmd,size_t s){
	if(s <= 0)
		return NULL;
	
	if(s > sizeof(paquete)-2)
		return NULL;
		
	paquete * temp = malloc(sizeof(paquete));
	memset(temp,0,sizeof(paquete));
	
	if(s <= 6){
		memcpy(temp,cmd,s);
		temp->magic = crc_ccitt_ffff(temp,sizeof(paquete));
		return temp;
	}
	
	memcpy(temp,cmd,6);
	
	unsigned char *t = &(temp->ud);
	memcpy(t,cmd+6,s-6);

	temp->magic = crc_ccitt_ffff(temp,sizeof(paquete));
	
	return temp;
}



int do_ping(libusb_device_handle **hand){
	unsigned char c = 0x0c;
	return send_cmd(hand,&c,1);
}
