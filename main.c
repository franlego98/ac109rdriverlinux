/*
 * main.c
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R v1.0
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

#include <stdio.h>
#include <string.h>
#include "parameters.h"
#include "tools.h"
#include "tools_stream.h"
#include "load_profile.h"

libusb_device_handle *h;

void usage(){
	printf("Keyboard ACGAM AG-109R Light Driver for Linux v1.0\n ");
	printf("\n Usage:\n");
	printf("\t set-profile   N\t\t  -  Select profile N in the keyboard\n");
	printf("\t clear-profile N\t\t  -  Clear profile N in the keyboard\n");
	printf("\t load-profile  N json_file\t  -  Load $json_file in profile N\n");
	printf("\t fill-profile  N B|{R G B}\t  -  Fill all profile N with brighness B or\n");
	printf("\t\t\t\t\t     with color R G B\n");
	printf("\n");
}

int set_profile(int n){
	if(n < 1 || n > 3){
		printf("Profile must be between 1 and 3\n");
		return 0;
	}
		
	unsigned char s[2];
	s[0] = 0x0b;
	s[1] = 0x01 + n;
	
	return send_cmd(&h,&s,2);
}

int clear_profile(int n){
	if(n < 1 || n > 3){
		printf("Profile must be between 1 and 3\n");
		return 0;
	}
		
	unsigned char s[3];
	s[0] = 0x21;
	s[1] = 0x01 + n;
	s[2] = 0x06;
	
	return send_cmd(&h,&s,3);
}

int send_profile(int p,const keyboard_key *lista){
	
	int res = 0;
	
	stream_data s;
	clear_profile(p);	
	stream_init(&s,p);
	
	unsigned char ini_t[] = {0x0,0x2,0x0,0x0,0x1,
							0x0,0x0,0x0,0x0,0x0,
							0x0,0x0,0x0,0x0,0x0,
							0x0,0x14,0x4,0x0,0x0,
							0x1,0x0,0x0,0x0,0x2e,
							0x4,0x0,0x0,0xd,0x0,
							0x0,0x0,0xce,0x5,0x0,
							0x0,0xc,0x0,0x0,0x0,
							0x6,0x7,0x0,0x0,0x3,
							0x0,0x0,0x0};
							
	stream_send(&h,&s,&ini_t,48);
	
							
	unsigned char key_base[] = {0xff,0xff,0xff,0xff};
			
	for(int i = 0;i < 116 ;i++){
		stream_send(&h,&s,&key_base,4);
	}
	
	unsigned char middle_t[] = {0x0,0x0,0x10,0x2};
	stream_send(&h,&s,&middle_t,4);
	
	for(int i = 0;i < KEYBOARD_NUM_KEYS;i++){	
		stream_send(&h,&s,&lista[i],sizeof(keyboard_key));
	}
	
	unsigned char end_t[] = {0x00};
	
	for(int i = 0;i < 1894 ;i++){
		stream_send(&h,&s,&end_t,1);
	}
	
	res = stream_end(&h,&s);
	
	set_profile(p);

	return res;
}

int load_profile(int p,const char *filename){
	
	int res = 0;
	
	keyboard_key teclado[KEYBOARD_NUM_KEYS];
	memset(&teclado,0,sizeof(keyboard_key)*KEYBOARD_NUM_KEYS);
					
	res = parseJson(filename,&teclado);
	if(res < 0){
		printf("Problem reading %s \n",filename);
		return -1;
	}

	return send_profile(p,&teclado);
}

int fill_profile(int p,int red,int green,int blue){
	int res = 0;
	
	keyboard_key teclado[KEYBOARD_NUM_KEYS];
	
	keyboard_key *temp;
	for(int i = 0;i < KEYBOARD_NUM_KEYS;i++){
		temp = &teclado[i];
		temp->red = red & 0xff;
		temp->green = green & 0xff;
		temp->blue = blue & 0xff;
		temp->alpha = 0xff;
	}
					
	return send_profile(p,&teclado);
}

int main(int argc,char **argv){
	int res = 0;
	
	if(init_usb(&h) < 0){
		printf("Problem initializating!\n");
		exit(-1);
	}
	
	if(do_ping(&h)<0){
		printf("Connected but not responding\n");
	}
	
	if(argc < 2){
		usage();
	}else{
		if(strcasecmp(argv[1],"set-profile") == 0){
			if(argc < 3){
				printf("Profile not specified!\n");
				exit(0);
			}else{
				res = set_profile(atoi(argv[2]));
			}
			goto exit;
		}
		if(strcasecmp(argv[1],"clear-profile") == 0){
			if(argc < 3){
				printf("Profile not specified!\n");
				exit(0);
			}else{
				res = clear_profile(atoi(argv[2]));
			}
			goto exit;
		}
		if(strcasecmp(argv[1],"load-profile") == 0){
			if(argc < 4){
				printf("Few arguments\n");
				exit(0);
			}else{
				res = load_profile(atoi(argv[2]),argv[3]);
			}
			goto exit;
		}
		if(strcasecmp(argv[1],"fill-profile") == 0){
			int red,green,blue;
			
			if(argc < 4){
				printf("Few arguments\n");
				exit(0);
			}else if(argc >= 6){
				red = atoi(argv[3]);
				green = atoi(argv[4]);
				blue = atoi(argv[5]);
			}else{
				red = green = blue = atoi(argv[3]);
			}
			
			if(red > 255 | green > 255 | blue > 255){
				printf("Values should be beetween 0 and 255\n");
			}
			res = fill_profile(atoi(argv[2]),red,green,blue);
			
			goto exit;
		}
		usage();
	}

exit:	
	if(res < 0){
		printf("Fail\n");
	}
	
	close_usb(&h);
	
	return 0;
}
