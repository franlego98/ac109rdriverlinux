CC = gcc
LIBS = -lusb-1.0 -ljansson
OBJS = tools.c tools_stream.c load_profile.c

all: *.c
	$(CC) $(LIBS) -o acgamd main.c $(OBJS)
