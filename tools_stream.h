/*
 * tools_stream.h
 * 
 * Program to maniputalte RGB Keys of ACGAM AG-109R
 * Written by Francisco Sanchez Lopez 2019
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

#ifndef _TOOLS_STREAM_H
#define _TOOLS_STREAM_H 

#include "tools.h"

#define STREAM_BUFF_MAX	56
 
typedef struct {
	int sent;
	int buff_filled;
	unsigned char buff[STREAM_BUFF_MAX];
	int profile;
} stream_data;
 
int stream_init(stream_data *s,int profile);

int stream_send(libusb_device_handle **handle,stream_data *s,unsigned char * msg,int siz);

int stream_end(libusb_device_handle **handle,stream_data *s);

#endif _TOOLS_STREAM_H
